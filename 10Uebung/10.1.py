# Importieren der OpenCV-Bibliothek
import cv2
import time
import matplotlib.pyplot as plt
import numpy as np

# Öffnen einer Videoquelle, hier einer Videodatei
# Argument: (Pfadname+)Dateiname der Videodatei
# ACHTUNG: Je nach Betriebssystem muss der Pfad eventuell unterschiedlich
# angegeben werden. Ausserdem ist zu beachten, von welchem Verzeichnis aus
# nach der Datei gesucht wird!
vid_file_name = 'file_0001_ts.avi'  # Beispielvideo aus Stud.iP
sums = [0]
sumsavg = [0]
sumslen = [0]
count1 = 0
myframe1 = 0
framecount = 0
fix, (ax1, ax2) = plt.subplots(2, 1)
ax1.set_title('Frame Data')
ax1.set_xlabel('Pixel Index')
ax1.set_ylabel('Pixel Value')
ax2.set_title('Sum of Pixel Values over Frames')
ax2.set_xlabel('Frame Index')
ax2.set_ylabel('Sum of Pixel Values')
hans_plot, = ax1.plot(count1, myframe1)
hans2_plot, = ax2.plot(sumslen[0], sums[0])
file_vid = cv2.VideoCapture(vid_file_name)
if file_vid.isOpened():
    file_vid_present = True
    print('Videodatei vorhanden')
else:
    file_vid_Present = False
    print('Videodatei nicht vorhanden')

while True:
    # Jetzt lesen wir ein Bild/Frame aus der Datei ein
    ret, frame = file_vid.read(cv2.IMREAD_GRAYSCALE)

    if ret:
        # Im Erfolgsfall zeigen wir das Bild an ...
        grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        myframe = grayFrame[149]
        sum = 0
        for i in myframe:
            sum+= i
        
        sums.append(sum)
        sumslen.append(len(sums))
        sumsavg.append(sum / len(sums))
        count = np.linspace(0,len(myframe),len(myframe))
        if framecount % 5 == 0:
                
            ax1.clear()
            ax1.plot(count, myframe)
            
            ax1.set_xlim(0, len(myframe))
            ax1.set_ylim(np.min(myframe), np.max(myframe)+100)
            plt.pause(0.1)
            
            ax2.clear()
            ax2.plot(sumslen, sums)
            
        
        # ... und zeigen dieses dann auch an
        cv2.imshow('grayFrame', grayFrame)

        # ... und damit das auch wirklich passiert, müssen wir die Tastatur
        # pollen (ähnlich wie 'plt.show()' bei der Matplotlib)
        if cv2.waitKey(1) & 0xFF == ord('q'):
        # Wenn wir 'q' drücken bricht unser Programm ab.
            break
    else:
        break
    framecount +=1
    # Damit das Video ungefähr in Echtzeit läuft, müssen wir etwas "bremsen"
    
    
# Jetzt geben wir das Videobjekt wieder frei ...
file_vid.release()

print('Videodatei geschlossen')

# ... und schließen alle Fenster.
cv2.destroyAllWindows()