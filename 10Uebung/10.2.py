# Importieren der OpenCV-Bibliothek
import cv2
import time
import matplotlib.pyplot as plt
import numpy as np

# Öffnen einer Videoquelle, hier einer Videodatei
# Argument: (Pfadname+)Dateiname der Videodatei
# ACHTUNG: Je nach Betriebssystem muss der Pfad eventuell unterschiedlich
# angegeben werden. Ausserdem ist zu beachten, von welchem Verzeichnis aus
# nach der Datei gesucht wird!
vid_file_name = 'file_0001_ts.avi'  # Beispielvideo aus Stud.iP
sums = [0]
sumsavg = [0]
sumslen = [0]

framecount = 0
fix, (ax1, ax3, ax2) = plt.subplots(3, 1)
ax1.set_title('Frame Data')
ax1.set_xlabel('Pixel Index')
ax1.set_ylabel('Pixel Value')
ax2.set_title('Sum of Pixel Values over Frames')
ax2.set_xlabel('Frame Index')
ax2.set_ylabel('Sum of Pixel Values')
ax3.set_title("filtered Frame Data")
ax3.set_xlabel("Pixel Index")
ax3.set_ylabel("Pixel Value")
hans_plot, = ax1.plot(0, 0)
hans2_plot, = ax2.plot(sumslen[0], sums[0])
hans3_plot, = ax3.plot(0, 0)
file_vid = cv2.VideoCapture(vid_file_name)
if file_vid.isOpened():
    file_vid_Present = True
    print('Videodatei vorhanden')
else:
    file_vid_Present = False
    print('Videodatei nicht vorhanden')

def getFilter():
    Filterk = []
    while True:
        a = input("Geben Sie einzelne int werte an und bestätigen sie diese mit Enter, wenn ende dann q eingeben\n")
        if a == "q":
            return Filterk
        else:   
            Filterk.append(int(a))
        

def TP_Filter(inFrame):
    FLength = int(input("please Enter the Filterlength for the LF Filter\n"))
    R = int((FLength - 1) / 2)
    outframe = np.zeros(len(inFrame))
    avgoutframe = np.zeros(len(inFrame))
    
    for inst in range(R, len(inFrame) - R):
        for x in range(-R, R + 1):
            outframe[inst - R] += inFrame[inst + x]
        avgoutframe[inst - R] = outframe[inst - R] / FLength
    
    return avgoutframe
        
def megaFilter(inFrame, Filterk):
    UFLength = len(Filterk)
    #determine Filterlength
    R = int((UFLength - 1) / 2)
    #determine Rand
    outframe = np.zeros(len(inFrame))
    #generate empty filtered Arr
    avgoutframe = np.zeros(len(inFrame))
    
    for inst in range(R, len(inFrame) - R):
        #apply the given filter for every set of elements till end - R
        for x in range (0, UFLength):
            
            outframe[inst] += inFrame[inst - R + x] * Filterk[x]

        avgoutframe[inst] = outframe[inst] / UFLength
    return avgoutframe
    
    
Filterk = getFilter()
while True:
    # Jetzt lesen wir ein Bild/Frame aus der Datei ein
    ret, frame = file_vid.read(cv2.IMREAD_GRAYSCALE)

    if ret:
        # Im Erfolgsfall zeigen wir das Bild an ...
        grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        myframe = grayFrame[149]
        sum = 0
        for i in myframe:
            sum+= i
        
        sums.append(sum)
        sumslen.append(len(sums))
        sumsavg.append(sum / len(sums))
        count = np.linspace(0,len(myframe),len(myframe))
        if framecount % 5 == 0:
                
            ax1.clear()
            ax1.plot(count, myframe)
            
            ax1.set_xlim(0, len(myframe))
            ax1.set_ylim(np.min(myframe), np.max(myframe)+100)
            plt.pause(0.1)
            
            ax2.clear()
            ax2.plot(sumslen, sums)
            #here is filtered now
            Newframe = megaFilter(myframe, Filterk)
            ax3.clear()
            ax3.plot(count, Newframe)
        
        # ... und zeigen dieses dann auch an
        cv2.imshow('grayFrame', grayFrame)

        # ... und damit das auch wirklich passiert, müssen wir die Tastatur
        # pollen (ähnlich wie 'plt.show()' bei der Matplotlib)
        if cv2.waitKey(1) & 0xFF == ord('q'):
        # Wenn wir 'q' drücken bricht unser Programm ab.
            break
    else:
        break
    framecount +=1
    # Damit das Video ungefähr in Echtzeit läuft, müssen wir etwas "bremsen"
    
    
# Jetzt geben wir das Videobjekt wieder frei ...
file_vid.release()

print('Videodatei geschlossen')

# ... und schließen alle Fenster.
cv2.destroyAllWindows()