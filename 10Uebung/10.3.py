import cv2
import matplotlib.pyplot as plt
import numpy as np

# Videodatei Name
vid_file_name = 'file_0001_ts.avi'

sums = []
sumsavg = []
sumslen = []

framecount = 0
fix, (ax1, ax3, ax2) = plt.subplots(3, 1)
ax1.set_title('Frame Data')
ax1.set_xlabel('Pixel Index')
ax1.set_ylabel('Pixel Value')
ax2.set_title('Sum of Pixel Values over Frames')
ax2.set_xlabel('Frame Index')
ax2.set_ylabel('Sum of Pixel Values')
ax3.set_title("Filtered Frame Data")
ax3.set_xlabel("Pixel Index")
ax3.set_ylabel("Pixel Value")
hans_plot, = ax1.plot(0, 0)
hans2_plot, = ax2.plot([], [])
hans3_plot, = ax3.plot(0, 0)
file_vid = cv2.VideoCapture(vid_file_name)
if file_vid.isOpened():
    file_vid_Present = True
    print('Videodatei vorhanden')
else:
    file_vid_Present = False
    print('Videodatei nicht vorhanden')

def getFilter():
    Filterk = []
    while True:
        a = input("Geben Sie einzelne int Werte an und bestätigen Sie diese mit Enter, wenn Ende, dann q eingeben\n")
        if a == "q":
            return Filterk
        else:
            try:
                Filterk.append(int(a))
            except ValueError:
                print("Bitte geben Sie eine gültige Ganzzahl ein.")

def TP_Filter(inFrame):
    try:
        FLength = int(input("Bitte geben Sie die Filterlänge für den TP-Filter ein\n"))
    except ValueError:
        print("Bitte geben Sie eine gültige Ganzzahl ein.")
        return inFrame

    R = int((FLength - 1) / 2)
    outframe = np.zeros(inFrame.shape)
    
    for z in range(inFrame.shape[0]):
        for y in range(R, inFrame.shape[1] - R):
            for x in range(R, inFrame.shape[2] - R):
                outframe[z, y, x] = np.mean(inFrame[z, y-R:y+R+1, x-R:x+R+1])
    
    return outframe

def megaFilter(inFrame, Filterk):
    UFLength = len(Filterk)
    R = int((UFLength - 1) / 2)
    outframe = np.zeros(inFrame.shape)
    
    for z in range(inFrame.shape[0]):
        for y in range(R, inFrame.shape[1] - R):
            for x in range(R, inFrame.shape[2] - R):
                outframe[z, y, x] = np.sum(inFrame[z, y-R:y+R+1, x-R:x+R+1] * Filterk)
    
    return outframe / UFLength

Filterk = getFilter()
frames = []

while True:
    ret, frame = file_vid.read()
    if ret:
        grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frames.append(grayFrame)
    else:
        break

frames = np.array(frames)
sum_vals = np.sum(frames, axis=(1, 2))
sums.extend(sum_vals)
sumslen.extend(range(len(sums)))
sumsavg.extend(sum_vals / frames.shape[1] / frames.shape[2])

count = np.linspace(0, frames.shape[2], frames.shape[2])

for z in range(frames.shape[0]):
    if z % 5 == 0:
        ax1.clear()
        ax1.plot(count, frames[z, 149, :])
        ax1.set_xlim(0, frames.shape[2])
        ax1.set_ylim(np.min(frames[z, 149, :]), np.max(frames[z, 149, :]) + 100)
        plt.pause(0.1)

        ax2.clear()
        ax2.plot(sumslen[:len(sums)], sums)

        newframe = megaFilter(frames, Filterk)
        ax3.clear()
        ax3.plot(count, newframe[z, 149, :])

file_vid.release()
print('Videodatei geschlossen')
cv2.destroyAllWindows()
