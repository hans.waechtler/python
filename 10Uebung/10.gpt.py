import cv2
import matplotlib.pyplot as plt
import numpy as np

# Videodatei Name
vid_file_name = 'file_0001_ts.avi'

sums = [0]
sumsavg = [0]
sumslen = [0]

framecount = 0
fix, (ax1, ax3, ax2) = plt.subplots(3, 1)
ax1.set_title('Frame Data')
ax1.set_xlabel('Pixel Index')
ax1.set_ylabel('Pixel Value')
ax2.set_title('Sum of Pixel Values over Frames')
ax2.set_xlabel('Frame Index')
ax2.set_ylabel('Sum of Pixel Values')
ax3.set_title("filtered Frame Data")
ax3.set_xlabel("Pixel Index")
ax3.set_ylabel("Pixel Value")
hans_plot, = ax1.plot(0, 0)
hans2_plot, = ax2.plot(sumslen[0], sums[0])
hans3_plot, = ax3.plot(0, 0)
file_vid = cv2.VideoCapture(vid_file_name)
if file_vid.isOpened():
    file_vid_Present = True
    print('Videodatei vorhanden')
else:
    file_vid_Present = False
    print('Videodatei nicht vorhanden')

def getFilter():
    Filterk = []
    while True:
        a = input("Geben Sie einzelne int werte an und bestätigen sie diese mit Enter, wenn ende dann q eingeben\n")
        if a == "q":
            return Filterk
        else:
            try:
                Filterk.append(int(a))
            except ValueError:
                print("Bitte geben Sie eine gültige Ganzzahl ein.")

def TP_Filter(inFrame):
    try:
        FLength = int(input("please Enter the Filterlength for the LF Filter\n"))
    except ValueError:
        print("Bitte geben Sie eine gültige Ganzzahl ein.")
        return inFrame

    R = int((FLength - 1) / 2)
    outframe = np.zeros(len(inFrame))
    avgoutframe = np.zeros(len(inFrame))
    
    for inst in range(R, len(inFrame) - R):
        for x in range(-R, R + 1):
            outframe[inst - R] += inFrame[inst + x]
        avgoutframe[inst - R] = outframe[inst - R] / FLength
    
    return avgoutframe
        
def megaFilter(inFrame, Filterk):
    UFLength = len(Filterk)
    R = int((UFLength - 1) / 2)
    outframe = np.zeros(len(inFrame))
    avgoutframe = np.zeros(len(inFrame))
    
    for inst in range(R, len(inFrame) - R):
        for x in range(0, UFLength):
            outframe[inst] += inFrame[inst - R + x] * Filterk[x]
        avgoutframe[inst] = outframe[inst] / UFLength
    return avgoutframe

Filterk = getFilter()
while True:
    ret, frame = file_vid.read()
    if ret:
        grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        myframe = grayFrame[149]
        sum_val = np.sum(myframe)
        
        sums.append(sum_val)
        sumslen.append(len(sums))
        sumsavg.append(sum_val / len(sums))
        count = np.linspace(0, len(myframe), len(myframe))
        
        if framecount % 5 == 0:
            ax1.clear()
            ax1.plot(count, myframe)
            ax1.set_xlim(0, len(myframe))
            ax1.set_ylim(np.min(myframe), np.max(myframe) + 100)
            plt.pause(0.1)
            
            ax2.clear()
            ax2.plot(sumslen, sums)
            
            Newframe = megaFilter(myframe, Filterk)
            ax3.clear()
            ax3.plot(count, Newframe)
        
        cv2.imshow('grayFrame', grayFrame)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break
    framecount += 1

file_vid.release()
print('Videodatei geschlossen')
cv2.destroyAllWindows()
