# Importieren der OpenCV-Bibliothek
import cv2

# Öffnen einer Videoquelle, hier einer Kamera
# Argument: laufende Nummer der Kamera des jeweiligen Systems
# - eingebaute Kamera (Notebook): 0
# - externe Kamera (Notebook mit Kamera): 1
# - externe Kamera (PC): 0
# Linux/MacOS:
#vid = cv2.VideoCapture(1)
# WINDOWS: Hier muss die Option für DIRECTSHOW mit angegeben werden,
# sonst dauert das Öffnen der Kamera ewig!
vid = cv2.VideoCapture(1+cv2.CAP_DSHOW)

print('cam opened')

while True:
    # Jetzt lesen wir ein Bild/Frame von der Kamera ein
    ret, frame = vid.read()

    if ret:
      # Im Erfolgsfall zeigen wir das Bild an ...
      cv2.imshow('Frame', frame)

      # ... und damit das auch wirklich passiert, müssen wir die Tastatur
      # pollen (ähnlich wie 'plt.show()' bei der Matplotlib)
      if cv2.waitKey(1) & 0xFF == ord('q'):
        # Wenn wir 'q' drücken bricht unser Programm ab.
        break
    else:
        break

# Jetzt geben wir das Videobjekt wieder frei ...
vid.release()

print('cam closed')

# ... und schließen alle Fenster.
cv2.destroyAllWindows()