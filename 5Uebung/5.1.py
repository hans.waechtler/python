import numpy as np
import matplotlib.pyplot as plt
def calcfunc():
    
    x = np.linspace(0, 2*np.pi, 100)
    sin_x = np.sin(x)
    sin3_x = np.sin(3*x)
    sin2_x = np.sin(x) * np.sin(x)
    return x, sin_x, sin2_x, sin3_x
def plotting(x, sin_x, sin2_x, sin3_x):
    (fig, axs) = plt.subplots(4)
    print(sin2_x)
    axs[0].plot(x, sin_x, "r")
    axs[1].set_title("es läuft")
    axs[1].plot(x, sin3_x, "b--")
    axs[2].plot(x, sin2_x, linestyle="none", color="black", marker="+")
    axs[3].plot(x, sin2_x)
    axs[3].plot(x, sin_x)
    axs[3].grid()
    axs[3].set_xlabel("eins")
    axs[3].set_ylabel("zwei")
    axs[3].set_title("jooooo")
    plt.show()
    
x, sin_x, sin2_x, sin3_x = calcfunc()
plotting(x, sin_x, sin2_x, sin3_x)