import numpy as np
import matplotlib.pyplot as plt

def calcfunc():
    x = np.linspace(0, 2*np.pi, 100)
    sin_x = np.sin(x)
    sin3_x = np.sin(3*x)
    sin2_x = np.sin(x) * np.sin(x)
    
    # Neue Funktionen
    func1 = np.sin(10*x) * np.exp(-0.5*x)
    func2 = np.exp(-0.5*x)
    func3 = -np.exp(-0.5*x)
    
    return x, sin_x, sin2_x, sin3_x, func1, func2, func3

def plotting(x, sin_x, sin2_x, sin3_x, func1, func2, func3):
    fig, axs = plt.subplots(4, 1, figsize=(8, 12))  # 4 Zeilen, 1 Spalte, angepasstes Größenverhältnis
    print(sin2_x)
    
    # Erstes Diagramm
    axs[0].plot(x, sin_x, "r")
    axs[0].set_title("sin(x)")
    
    # Zweites Diagramm
    axs[1].plot(x, sin3_x, "b--")
    axs[1].set_title("sin(3x)")
    
    # Drittes Diagramm
    axs[2].plot(x, sin2_x, linestyle="none", color="black", marker="+")
    axs[2].set_title("sin(x) * sin(x)")
    
    # Viertes Diagramm
    axs[3].plot(x, sin2_x, label='sin(x) * sin(x)')
    axs[3].plot(x, sin_x, label='sin(x)')
    axs[3].plot(x, func1, label='sin(10x) * exp(-0.5x)', color='blue', linestyle='-')
    axs[3].plot(x, func2, label='+exp(-0.5x)', color='blue', linestyle='--')
    axs[3].plot(x, func3, label='-exp(-0.5x)', color='blue', linestyle='--')
    axs[3].grid()
    axs[3].set_xlabel("x")
    axs[3].set_ylabel("y")
    axs[3].set_title("Zusätzliche Funktionen")
    axs[3].legend()
    
    plt.tight_layout()
    plt.show()
    
# Die Rückgabe von calcfunc entpacken und an plotting übergeben
x, sin_x, sin2_x, sin3_x, func1, func2, func3 = calcfunc()
plotting(x, sin_x, sin2_x, sin3_x, func1, func2, func3)
