import numpy as np
import matplotlib.pyplot as plt

def simulate_motion(time_intervals, control_signals):
    num_steps = len(time_intervals)
    times = np.zeros(num_steps)
    accelerations = np.zeros(num_steps)
    velocities = np.zeros(num_steps)
    positions = np.zeros(num_steps)
    
    accelerations[0] = control_signals[0]
    
    dt = np.diff(time_intervals)
    
    for i in range(1, num_steps):
        times[i] = time_intervals[i]
        accelerations[i] = control_signals[i]
        velocities[i] = velocities[i-1] + accelerations[i-1] * dt[i-1]
        positions[i] = positions[i-1] + velocities[i-1] * dt[i-1] + 0.5 * accelerations[i-1] * dt[i-1]**2
    
    return times, positions, velocities, accelerations, control_signals

def plot_motion(time_intervals, times, positions, velocities, accelerations, control_signals):
    fig, ax = plt.subplots(4, 1, figsize=(10, 12))

    ax[0].plot(times, positions, label='s(t)', color='blue')
    ax[0].set_xlabel('t')
    ax[0].set_ylabel('s')
    ax[0].legend()
    ax[0].grid(True)

    ax[1].plot(times, velocities, label='v(t)', color='green')
    ax[1].set_xlabel('t')
    ax[1].set_ylabel('v')
    ax[1].legend()
    ax[1].grid(True)

    ax[2].step(time_intervals, control_signals, where='post', label='a(t)', color='yellow')
    ax[2].set_xlabel('t')
    ax[2].set_ylabel('a')
    ax[2].legend()
    ax[2].grid(True)

    ax[3].stem(time_intervals, control_signals, label='u(t)', linefmt='red', basefmt='r-')
    ax[3].set_xlabel('t')
    ax[3].set_ylabel('u')
    ax[3].legend()
    ax[3].grid(True)

    plt.tight_layout()
    plt.show()

# Example data
time_intervals = np.array([0.0, 15.5, 30.5, 41.0, 50.0, 60.0])
control_signals = np.array([1.0, 0.0, 0.5, -2.0, 1.5, 0.0])

times, positions, velocities, accelerations, control_signals = simulate_motion(time_intervals, control_signals)
plot_motion(time_intervals, times, positions, velocities, accelerations, control_signals)
