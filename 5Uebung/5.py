import numpy as np
import matplotlib.pyplot as plt

x = np.zeros(100, dtype = np.float32)
# x = np.linspace(0, 2*np.pi, 100)
sin_x = np.zeros(100, dtype=np.float32)

dx = 2*np.pi / 100

curr_x = 0.0

for i_n in range (0,100):
    x[i_n] = curr_x
    sin_x[i_n] = np.sin(x[i_n])
    curr_x += dx
# sin_x = np.sin(x)
    
(fig, ax) = plt.subplots()

ax.plot(x, sin_x)

plt.show()