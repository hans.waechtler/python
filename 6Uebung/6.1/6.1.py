import numpy as np
import matplotlib.pyplot as plt
import config as cfg

tvons = []
vsollgroß = []
tvonsgroß = np.linspace(0,1200, 1200)
vsoll = []
vist = [0]
weg = [0]
acc = [0]
filename = cfg.configuration["filename"]
fd = open(filename, "r")
Lines = fd.readlines()

for line in Lines:
    fileelements = line.split()
    tvons.append(float(fileelements[0]))
    vsoll.append(float(fileelements[1]))
    #print(tvons, vsoll)
    
listlength = len(tvonsgroß)

for k in range(0, listlength-1):
    for tk in np.arange(tvons[k]+cfg.configuration["dt"], tvons[k+1]+cfg.configuration["dt"], cfg.configuration["dt"]):
        next_weg = weg[-1] + cfg.configuration["dt"]*vist[-1]
        weg.append(next_weg)    
        next_vist = vist[-1] + (cfg.configuration["Kv"] * vsoll[k] - vist[-1]) * (cfg.configuration["dt"] / cfg.configuration["Tv"])
        vist.append(next_vist)
        
        next_acc = (vist[-1] - vist[-2]) / cfg.configuration["dt"]
        acc.append(next_acc)
        
    
fig, axe = plt.subplots()
tvons.insert(0, 0)
axe.plot(vist, tvonsgroß)
plt.show()

