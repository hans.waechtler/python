configuration = {
    "dt" : 0.05,
    "Kv" : 1,
    "Tv" : 2.5,
    "width" : 10,
    "height" : 12,
    "color" : "red",
    "linestyle" : ":",
    "filename" : "values"
}