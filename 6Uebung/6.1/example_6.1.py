import example_6_1_dict as ex6
import numpy as np
import matplotlib.pyplot as plt
def datei_fuellen(datei_name):
    fd = open(datei_name, 'w')
    data = np.array([[0, 12.5], [15.5, 0], [28.5, 31], [43, 22.5], [52, 0], [60, 0]])  
    np.savetxt(datei_name, data, delimiter=',', fmt='%f')       
    fd.close()

def datei_lesen(datei_name):
    fd = open(datei_name, 'r')
    data = np.loadtxt(datei_name, delimiter= ',')
    t_list = data[:, 0]
    vsoll_list = data[:, 1]
    print(t_list, vsoll_list)
    print(data)
    fd.close()
    return t_list, vsoll_list

def calculate(t_list, vsoll):
    vk_soll = [vsoll[0]]
    vk_ist = [0]
    ak_ist = [0]
    sk_ist = [0]
    for t in range(0, len(t_list)-1, 1):
        #if t == 0:
        #    # beginnen erst beim zeitschritt weil alles beginnt ja bei 0
        #    for tk in np.arange(ex6.model["Zeitschritt"], t_list[t+1], ex6.model["Zeitschritt"]):
        #        vk_soll.append(vsoll[t])
        #        sk_ist.append(sk_ist[-1]+ex6.model["Zeitschritt"]*vk_ist[-1])  #neue Strecke vor neuer geschwindikeit --> sonst rechnen wir mit vk+1
        #        vk_ist.append(vk_ist[-1]+(ex6.model["Verstärkungsfaktor"]*vsoll[t]-vk_ist[-1])*(ex6.model["Zeitschritt"]/ex6.model["Zeitkonstante"]))
        #        ak_ist.append((vk_ist[-1] - vk_ist[-2])/ex6.model["Zeitschritt"])
        #        #mit ak erst danach weil wir vk+1 zur berechnung benötigen
        #else:
            # machen genau das gleiche nur mit flexiblem start, gehen immer nur bis vor der nächsten Zeit, dann ist ja auch eine neue Geschwindigkeit
            for tk in np.arange(t_list[t]+ex6.model["Zeitschritt"], t_list[t+1]+ex6.model["Zeitschritt"], ex6.model["Zeitschritt"]):
                vk_soll.append(vsoll[t])
                sk_ist.append(sk_ist[-1]+ex6.model["Zeitschritt"]*vk_ist[-1])  #neue Strecke vor neuer geschwindikeit --> sonst rechnen wir mit vk+1
                vk_ist.append(vk_ist[-1]+(ex6.model["Verstärkungsfaktor"]*vsoll[t]-vk_ist[-1])*(ex6.model["Zeitschritt"]/ex6.model["Zeitkonstante"]))
                ak_ist.append((vk_ist[-1] - vk_ist[-2])/ex6.model["Zeitschritt"])
                #mit ak erst danach weil wir vk+1 zur berechnung benötigen
    
    return vk_soll, vk_ist, sk_ist, ak_ist


def plotten(tk_list, vk_s, vk_i, sk_i, ak_i):
    fig, axs = plt.subplots(3, 1, figsize=(ex6.plot['width'], ex6.plot['height']))
    axs[0].plot(tk_list, vk_s, label='vi(t)', color= ex6.plot['colour.vs'], linestyle = ex6.plot['linestyle.soll']) # so die einzelnen Plots ansprechen
    axs[0].set_title('v soll')
    axs[0].plot(tk_list, vk_i, label='vs(t)', color=ex6.plot['colour.vi'])       
    axs[0].set_title('v ist')
    axs[1].plot(tk_list, sk_i, label='s(t)', color=ex6.plot['colour.s'], linestyle = ex6.plot['linestyle.s.ist'], marker = ex6.plot['marker.s.ist'])
    axs[1].set_title('s ist')
    axs[2].plot(tk_list, ak_i, label='a(t)', color=ex6.plot['colour.a'])
    axs[2].set_title('a ist')
    for row in axs:
        row.set_xlabel('x')          #für jedes Plot die Achsen beschriften und formatieren und Legende machen
        row.set_ylabel('y')
        row.grid()
        row.legend()
    plt.tight_layout()  # Platzierung der Subplots optimieren
    # und anzeigen
    plt.show()



#datei erstellen
print(ex6.data["dataname"])
datei_fuellen(ex6.data["dataname"])
#datei auslesen
t_list, vsoll_list = datei_lesen(ex6.data["dataname"])
tk_list= np.arange(0, t_list[-1]+ex6.model["Zeitschritt"], ex6.model["Zeitschritt"])
print(tk_list)
vk_s, vk_i, sk_i, ak_i = calculate(t_list, vsoll_list)  #tk wird erst beim plotten gebraucht, vorher nicht
print(vk_s)
plotten(tk_list, vk_s, vk_i, sk_i, ak_i)