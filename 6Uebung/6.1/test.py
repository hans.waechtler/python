import numpy as np
import matplotlib.pyplot as plt
import config as cfg

# Initialize lists
tvons = []
vsoll = []
vsollgroß = []
vist = [0]
weg = [0]
acc = [0]

# Read the filename from configuration
filename = cfg.configuration["filename"]
with open(filename, "r") as fd:
    Lines = fd.readlines()

# Read data from file
for line in Lines:
    fileelements = line.split()
    tvons.append(float(fileelements[0]))
    vsoll.append(float(fileelements[1]))

# Generate tvonsgroß based on the given time range with step dt
tvonsgroß = np.arange(0, tvons[-1] + cfg.configuration["dt"], cfg.configuration["dt"])

# Perform the calculations
for t in range(0, len(tvons) - 1):
    for tk in np.arange(tvons[t], tvons[t + 1], cfg.configuration["dt"]):
        vsollgroß.append(vsoll[t])
        next_vist = vist[-1] + (cfg.configuration["Kv"] * (vsoll[t] - vist[-1])) * (cfg.configuration["dt"] / cfg.configuration["Tv"])
        vist.append(next_vist)
        next_weg = weg[-1] + cfg.configuration["dt"] * vist[-1]
        weg.append(next_weg)
        next_acc = (vist[-1] - vist[-2]) / cfg.configuration["dt"]
        acc.append(next_acc)
vsollgroß.append(vsoll[-1])  # Ensure vsollgroß matches the length of tvonsgroß

# Trim tvonsgroß to match the length of the calculated lists
tvonsgroß = tvonsgroß[:len(vsollgroß)]

# Plotting
fig, (ax1, ax2, ax3) = plt.subplots(3, 1)

# Plot actual velocity (v_ist) and desired velocity (v_soll)
ax1.plot(tvonsgroß, vsollgroß, 'b:', label='v_soll')  # Plot v_soll as a step function
ax1.plot(tvonsgroß, vist[1:], 'r-', label='v_ist')  # Plot v_ist as a smooth line
ax1.set_title('Velocity (v)')
ax1.set_xlabel('t [s]')
ax1.set_ylabel('v [m/s]')
ax1.legend()
ax1.grid(True)

# Plot acceleration (a)
ax2.plot(tvonsgroß, acc[1:], 'g-', label='a_ist')  # Plot a_ist as a green line
ax2.set_title('Acceleration (a)')
ax2.set_xlabel('t [s]')
ax2.set_ylabel('a [m/s²]')
ax2.legend()
ax2.grid(True)

# Plot distance (s)
ax3.plot(tvonsgroß, weg[1:], 'k-.', label='s_ist')  # Plot s_ist as a dashed black line
ax3.set_title('Distance (s)')
ax3.set_xlabel('t [s]')
ax3.set_ylabel('s [m]')
ax3.legend()
ax3.grid(True)

plt.tight_layout()
plt.show()
