import numpy as np
import matplotlib.pyplot as plt
import sys
import cfg_example_6_1

print(cfg_example_6_1.hallo)

mydict = {
    "Hans": 1,
    "Peter": 2
}

# Corrected np.linspace usage
hansarr = np.linspace(0, 100, 10)
myarr = np.linspace(0, 50, 10)

fig, ax = plt.subplots()

# Use dictionary to set figure size from the cfg module
fig.set_size_inches(cfg_example_6_1.plt_fig['width'], cfg_example_6_1.plt_fig['height'])
ax.plot(hansarr, myarr)
plt.show()

for a in mydict:
    print(a, mydict[a])
