"""
Iterative Berechnung der Sinusfunktion mit schritthaltender grafischer Ausgabe
"""
# Importieren der notwendigen Module
import numpy as np
import matplotlib.pyplot as plt

# Anzahl der Stützpunkte
n_gesamt = 200
# Darstellungsintervall
t_gesamt = 2.0*np.pi
# Inkrement zwischen 2 Stützpunkten
dt = t_gesamt / n_gesamt

# Vektor für Zeitwerte
t_vec = np.zeros(n_gesamt)
# Vektor für Ergebniswerte
sin_vec = np.zeros_like(t_vec)

# Plot anlegen ...
fig, ax = plt.subplots()

# ... und den ersten Iterationsschritt vorab berechnen
i=0
sin_vec[i] = np.sin(t_vec[i])

# Ersten Stützpunkt "normal" plotten ...
# Die ax.plot()-Funktion liefert uns eine Referenz auf die Plotdaten zurück
sin_plot, = ax.plot(t_vec[:i], sin_vec[:i])
# ... und das Diagramm groß genug machen für die weiteren Punkte
ax.set_xlim(-1,2.0*np.pi)
ax.set_ylim(-1.5,+1.5)

# Jetzt den Plot mit kurzer Pause ausgeben ohne anzuhalten ...
plt.pause(0.1)

# ... und jetzt die restlichen Stützpunkte iterativ berechnen und nach jedem
# 10. Schritt den Plot aktualisieren
for i in range(1,n_gesamt):
  t_vec[i] += t_vec[i-1]+dt
  sin_vec[i] = np.sin(t_vec[i])

  if i % 10 == 0:
    # Hier nutzen wir die Referenz auf die Plotdaten und können diese einfach
    # aktualisieren - die Darstellung wird automatisch ...
    sin_plot.set_data(t_vec[:i],sin_vec[:i])
    # ... mit aktualisiert, wenn wir eine kurze Pause einlegen
    plt.pause(0.1)