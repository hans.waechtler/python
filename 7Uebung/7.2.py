import numpy as np
import matplotlib.pyplot as plt
import example_6_1_dict as ex6

def inputfile():
    filename = ex6.data["dataname"]
    fd = open(filename, "r")
    Lines = fd.readlines()
    v_soll = []
    t_raw = []
    for line in Lines:
        Lineelements = line.split()
        t_raw.append(float(Lineelements[0]))
        v_soll.append(float(Lineelements[1]))
    return v_soll, t_raw

class MotionPlot():
    def __init__(self):
        self.fig, self.axs = plt.subplots(3, 1)
    
    
    def init_plot(self, t_vec, vneu_soll, v_ist, a_ist, s_ist):
        self.my_plot, = self.axs[0].plot(t_vec, v_ist, label='vi(t)', color= "red")
        self.my_plot23, = self.axs[0].plot(t_vec, vneu_soll, linestyle=":")

        self.axs[0].set_xlim(-1, 60)
        self.axs[0].set_ylim(-1.5, 60)

        self.my2_plot, = self.axs[1].plot(t_vec, s_ist)

        self.axs[1].set_xlim(-1, 60)
        self.axs[1].set_ylim(-1.5, 100)

        self.my3_plot, = self.axs[2].plot(t_vec, a_ist)

        self.axs[2].set_xlim(-1, 60)
        self.axs[2].set_ylim(-1.5, 1)
    #ToDo:
    #- Plot initial ausgeben
    #- Plotreferenz merken

    def update_plot(self, t_vec, vneu_soll, v_ist, a_ist, s_ist):
        self.my_plot.set_data(t_vec, v_ist)
        self.my_plot23.set_data(t_vec, vneu_soll)
        self.my2_plot.set_data(t_vec, s_ist)
        self.my3_plot.set_data(t_vec, a_ist)
    
    #ToDo:
    #- Plotdaten aktualisieren mit neuen Daten

# Klasse zur Repräsentation der Objektbewegung
# (Berechnung von v_ist, a_ist, s_ist)
class MotionObject():
    def set_v_soll(self, v_soll):

        self.v_soll = v_soll
    
    
    
    #ToDo:
    #- neues v_soll setzen

    def move(self, dt, v_ist, a_ist, s_ist):
        self.s_ist = s_ist+dt*v_ist
        buffer_v_ist = v_ist
        self.v_ist = buffer_v_ist+(dt*self.v_soll-buffer_v_ist)*(dt/ex6.model["Zeitkonstante"])
        self.a_ist = (self.v_ist - buffer_v_ist) / dt
        return self.v_ist, self.a_ist, self.s_ist
    
    #sk_ist.append(sk_ist[-1]+ex6.model["Zeitschritt"]*vk_ist[-1])  #neue Strecke vor neuer geschwindikeit --> sonst rechnen wir mit vk+1
    #vk_ist.append(vk_ist[-1]+(ex6.model["Verstärkungsfaktor"]*vsoll[t]-vk_ist[-1])*(ex6.model["Zeitschritt"]/ex6.model["Zeitkonstante"]))
    #ak_ist.append((vk_ist[-1] - vk_ist[-2])/ex6.model["Zeitschritt"])
    #ToDo:
    #- Bewegungsgrößen für den Zeitschritt dt berechnen
    #- Ergebnisse in der Form  (self.v_ist, self.a_ist, self.s_ist) zurückgeben
    
    
if __name__ == "__main__":
    
    v_soll, t_short = inputfile()
    
    motion_objekt = MotionObject()
    motion_plot = MotionPlot()
    v_ist = np.zeros(1200)
    a_ist = np.zeros(1200)
    s_ist = np.zeros(1200)
    vneu_soll = np.zeros(1200)
    i = 1
    n_gesamt = int(t_short[-1] / ex6.model["Zeitschritt"])
    t_vec = np.linspace(0,60,1200)
    print(t_vec)
    motion_plot.init_plot(t_vec[0], v_soll[0], v_ist[0], a_ist[0], s_ist[0])
    plt.pause(0.1)
    for t in range(0, len(t_short)-1, 1):
        for tk in np.arange(t_short[t]+ex6.model["Zeitschritt"], t_short[t+1]+ex6.model["Zeitschritt"], ex6.model["Zeitschritt"]):
            motion_objekt.set_v_soll(v_soll[t])
            vneu_soll[i-1] = v_soll[t]
            v_ist[i] = motion_objekt.move(ex6.model["Zeitschritt"], v_ist[i-1], a_ist[i-1], s_ist[i-1])[0]
            a_ist[i] = motion_objekt.move(ex6.model["Zeitschritt"], v_ist[i-1], a_ist[i-1], s_ist[i-1])[1]
            s_ist[i] = motion_objekt.move(ex6.model["Zeitschritt"], v_ist[i-1], a_ist[i-1], s_ist[i-1])[2]
            i+=1
            if i % 5 == 0:
                motion_plot.update_plot(t_vec[:i], vneu_soll[:i], v_ist[:i], a_ist[:i], s_ist[:i])
                plt.pause(0.00000001)
            
                
    
        