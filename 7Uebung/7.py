# Wir definieren 2 Integervariablen ...
a = 42
b = int(73)

# ... und schauen uns deren Datentypen an ...
print('Datentyp von a=', type(a))
print('Datentyp von b=', type(b))

# ... addieren a und b "konventionell" ...
c = a + b
print('Datentyp von c=', type(c))

# ... und etwas "unkonventioneller" ...
d = a.__add__(b)
print('Datentyp von d=', type(d))

# Zur Kontrolle lassen wir uns noch einmal alle Variablen ausgeben.
# NEU: Hier sehen wir gleich ein Beispiel, wie man bei 'print()' die Ausgabe formatieren kann.
print('a=%d  b=%d  c=%c  d=%d' %(a,b,c,d))

# Zuerst definieren wir die Klasse, die Python sich - ebenso wie Funktionen - im Vorbeigehen für die spätere Verwendung "merkt"
class MeineKlasse():
  # ... die Methoden werden - wie üblich - eingerückt ...
  def __init__(self):
    # ... sind ansonsten aber ganz normale Python-Funktionen
    pass

# Hier beginnt das eigentliche "Hauptprogramm" mit der Definition einer Instanz unserer Klasse
mein_objekt = MeineKlasse()
print('Datentyp von m_objekt=', type(mein_objekt))

# Definition der Klasse
class Komplex():
  # Konstruktor mit Defaultwerten für die Argumente
  def __init__(self, real=0.0, imag=0.0):
    # Die Attribute der Klasse sollten im Konstruktor definiert werden
    self.real = real  # Realteil der komplexen Zahl
    self.imag = imag  # Imaginärteil der komplexen Zahl

# Eine Instanz mit Defaultwerten anlegen ...
k1 = Komplex()
print('Datentyp von k1=', type(k1))
print('Realteil=%f  Imaginärteil=%f' %(k1.real, k1.imag))

# ... und eine mit Übergabe der Initialisierungswerte
k2 = Komplex(-1.0, 1.5)
print('Datentyp von k2=', type(k2))
print('Realteil=%f  Imaginärteil=%f' %(k2.real, k2.imag))

class Komplex():
  # Konstruktor
  def __init__(self, real=0.0, imag=0.0):
    print('Objekt der Klasse \'Komplex\' angelegt')
    self.real = real
    self.imag = imag

  # Destruktor - wird bei der Zerstörung aufgerufen
  def __del__(self):
    print('Objekt der Klasse \'Komplex\' gelöscht')

  # Methode zur passenden print-Ausgabe
  def print(self):
    if self.imag >= 0.0:
      print('(%3.2f+j%3.2f)' %(self.real, self.imag))
    else:
      print('(%3.2f-j%3.2f)' %(self.real, abs(self.imag)))

  # Methode zum Addieren zweier komplexer Zahlen
  def add(self,ks):
    return Komplex(self.real+ks.real, self.imag+ks.imag)

#---------------------------------------------
# Hauptprogramm: Einstiegspunkt
if __name__ == '__main__':

  # 1. Instanz
  k1 = Komplex(2.0, -1.5)
  k1.print()

  # 2. Instanz
  k2 = Komplex(-1.0, 2.5)
  k2.print()

  # 3. Instanz als Ergebnis der Addition von k1 und k2
  k3 = k1.add(k2)
  k3.print()
#---------------------------------------------