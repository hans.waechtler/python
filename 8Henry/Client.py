import numpy as np
import socket 

# Unsere Klasse für die Sinus-Berechnung
class Sinus():
  def __init__(self, A=1.0, omega=1.0):
    # Amplitude
    self.A = A
    # Kreisfrequenz
    self.omega = omega

  # "Setter"-Funktion für die Amplitude
  def set_A(self, A):
    self.A = A

  # "Setter"-Funktion für die Kreisfrequenz
  def set_omega(self, omega):
    self.omega = omega

  # Berechnung des Sinus
  def calc(self,t):
    return self.A * np.sin(self.omega*t)

#---------------------------------------------
# Hauptprogramm: Einstiegspunkt
if __name__ == '__main__':
  
    msg_type = np.dtype([('id', 'U50'),
                      ('x',  np.float32),
                      ('y',  np.float32),
                      ('msg', 'U50')])
    
    localIP="127.0.0.1"
    localPort = 1234
    serverAdressPort=("127.0.0.1", 1234)
    bufferSize = 2048
    UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    data = np.zeros(1, dtype=msg_type)
    print("Client up and ready to send!")


    data['id']="dim"
    data['x']= 8
    data['y']= 4
    bytesToSend = data.tobytes()
    UDPClientSocket.sendto(bytesToSend, serverAdressPort)

    data['id']="xlim"
    data['x']= -0.5
    data['y']= 8
    bytesToSend = data.tobytes()
    UDPClientSocket.sendto(bytesToSend, serverAdressPort)

    data['id']="ylim"
    data['x']= -1.5
    data['y']= 1.5
    bytesToSend = data.tobytes()
    UDPClientSocket.sendto(bytesToSend, serverAdressPort)

    data['id']="colour"     #für die farbe
    data['msg']="r:"
    bytesToSend = data.tobytes()
    UDPClientSocket.sendto(bytesToSend, serverAdressPort)

    data['id']="leg_grid"
    data['x']= 1            #legende und grid beide an, x für legende, y für grid
    data['y']= 1
    data['msg'] = "sin(x)"
    bytesToSend = data.tobytes()
    UDPClientSocket.sendto(bytesToSend, serverAdressPort)

    # Anzahl der Stützpunkte
    n_gesamt = 1000
    # Darstellungsintervall
    t_gesamt = 2.0*np.pi
    # Inkrement zwischen 2 Stützpunkten
    dt = t_gesamt / n_gesamt
    # Vektor für Zeitwerte
    t_vec = np.zeros(n_gesamt)
    # Vektor für Ergebniswerte
    sin_vec = np.zeros_like(t_vec)
    # Sinus-Objekt anlegen ...
    sinus = Sinus()
    # Wir ergänzen ein paar Parameter für die Sinus-Funktion
    omega = 0.0
    domega = 0.025
    A = 1.0
    dA = -0.001
    # ... und den ersten Iterationsschritt vorab berechnen
    i=0
    sinus.set_A(A)
    sinus.set_omega(omega)
    sin_vec[i] = sinus.calc(t_vec[i])
    #senden das wir starten wollen und erstellen des Plot-Fensters:
    data['id']="1"
    data['x']=i*dt
    data['y']=sin_vec[i]
    bytesToSend = data.tobytes()
    UDPClientSocket.sendto(bytesToSend, serverAdressPort)
    # ... und jetzt die restlichen Stützpunkte iterativ berechnen und nach
    # jedem 10. Schritt den Plot aktualisieren
    for i in range(1,n_gesamt):
        t_vec[i] += t_vec[i-1]+dt
        # omega durch"wobbeln"
        omega += domega
        sinus.set_omega(omega)
        # A durch"wobbeln"
        A += dA
        sinus.set_A(A)
        sin_vec[i] = sinus.calc(t_vec[i])

        data['id']="2"
        data['x']=i*dt
        data['y']=sin_vec[i]
        bytesToSend = data.tobytes()
        if i % 1 == 0:
            UDPClientSocket.sendto(bytesToSend, serverAdressPort)

    data['id']="3"
    data['x']=0
    data['y']=0
    bytesToSend = data.tobytes()
    UDPClientSocket.sendto(bytesToSend, serverAdressPort)
    print("Client finished sending!")

#-  --------------------------------------------