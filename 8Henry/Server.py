import numpy as np
import matplotlib.pyplot as plt
import socket

# Unsere Klasse für die Plotausgabe
class SinPlot():
  # Konstruktor
  def __init__(self):
    # Plot anlegen ...
    self.fig, self.ax = plt.subplots()

  # Aufruf für die erste Plot-Ausgabe
  def init_plot(self, t_vec, sin_vec, xmin, xmax, ymin, ymax, h, w, color, legend, grid, label):
    # Ersten Stützpunkt "normal" plotten ...
    self.sin_plot, = self.ax.plot(t_vec, sin_vec, color, label= label)
    # ... und das Diagramm groß genug machen für die weiteren Punkte
    self.ax.set_xlim(xmin, xmax)
    self.ax.set_ylim(ymin, ymax)
    self.fig.set_size_inches(h,w)
    if (legend == 1):
       self.ax.legend()
    if(grid == 1):
       self.ax.grid()

  # Aufruf für die weiteren Plot-Ausgaben
  def update_plot(self, t_vec, sin_vec):
    self.sin_plot.set_data(t_vec,sin_vec)

if __name__ == '__main__':
    sinus = SinPlot()
    
    msg_type = np.dtype([('id', 'U50'),
                        ('x',  np.float32),
                        ('y',  np.float32),
                        ('msg', 'U50')])
    
    localIP = "127.0.0.1"
    localPort= 1234
    bufferSize = 2048
    UDPServerSocket = socket.socket(family = socket.AF_INET, type = socket.SOCK_DGRAM)
    UDPServerSocket.bind((localIP, localPort))
    print("Server Up and listening!")

    i=0
    sin_vec= []
    t_vec= []
    bytesAdressPair = UDPServerSocket.recvfrom(bufferSize)
    message = np.frombuffer(bytesAdressPair[0], dtype= msg_type)
    while(message['id'][0]!= "3"):
        if(message['id'][0]== "dim"):
            h = message['x'][0]
            w = message['y'][0]
        elif(message['id'][0]== "xlim"):
            xmin= message['x'][0]
            xmax= message['y'][0]
        elif(message['id'][0]== "ylim"):
            ymin= message['x'][0]
            ymax= message['y'][0]
        elif(message['id'][0]== "leg_grid"):
            legend= message['x'][0]
            grid= message['y'][0]
            label = message['msg'][0]
    
        elif(message['id'][0]== "1"):
            sin_vec.append(message['y'][0])
            t_vec.append(message['x'][0])
            sinus.init_plot(t_vec[:i], sin_vec[:i], xmin, xmax, ymin, ymax, h, w, color, legend, grid, label)
            #plt.pause(0.1)
            i = i+1

        elif(message['id'][0]== "2"):
            sin_vec.append(message['y'][0])
            t_vec.append(message['x'][0])
            if(i%10==0):
                sinus.update_plot(t_vec[:i], sin_vec[:i])
                print(i)
                plt.pause(0.01)
            i = i+1
            
        elif(message['id'][0] == "colour"):
           color = message['msg'][0]

        bytesAdressPair = UDPServerSocket.recvfrom(bufferSize)
        message = np.frombuffer(bytesAdressPair[0], dtype= msg_type)
    print("Der Server wurde beendet")