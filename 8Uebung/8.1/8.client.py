"""
Einfacher UDP-Client, der eine Nachricht sendet, auf eine Antwort sendet und sich dann beendet.
"""

# Für die Sockets gibt es natürlich ein eigenes Modul
import socket

# Wir nutzen das Loopback-Device zur rechnerinternen Kommunikation ...
localIP     = "127.0.0.1"
# ... und einen nicht belegten Port
localPort   = 1234
# IP und Port müssen wir als Tupel zusammenfassen
serverAddressPort   = ("127.0.0.1", 1234)

# Das ist die maximale Paketlänge für den Empfang ...
bufferSize  = 1024

# ... und das unsere Testnachricht ...
msgFromClient       = "Hello UDP Server"
# ... die wir in eine Byte-Folge konvertieren müssen
bytesToSend         = bytes(msgFromClient, 'UTF-8')

# Jetzt legen wir einen Socket für die UDP an ...
UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

print("UDP client up and sending")

# ... und schon können wir auf Senden gehen
UDPClientSocket.sendto(bytesToSend, serverAddressPort)

# ... und dann auf die Antwort warten
# Die Funktion recvfrom() kehrt nur zurück, wenn eine Nachricht empfangen wurde
# oder ein Fehler aufgetreten ist ...
bytesAddressPair = UDPClientSocket.recvfrom(bufferSize)
# ... und liefert uns im Erfolgsfall ein Tupel mit diesem Inhalt:
message = bytesAddressPair[0]
address = bytesAddressPair[1]

# Die Nachricht und die Absender-IP schauen wir uns mal an...
print('Message from Server: ', message)
print('Server IP Address: ' , address)