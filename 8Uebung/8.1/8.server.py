import socket
import numpy as np



# Wir nutzen das Loopback-Device zur rechnerinternen Kommunikation ...
localIP     = "192.168.1.150"
# ... und einen nicht belegten Port
localPort   = 1234
# Das ist die maximale Paketlänge für den Empfang ...
bufferSize  = 1024

# ... und das unsere Testnachricht ...
msgFromServer       = "Hello UDP Client"
# ... die wir in eine Byte-Folge konvertieren müssen
bytesToSend         = bytes(msgFromServer, 'UTF-8')

# Jetzt legen wir einen Socket für die UDP an ...
UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
# ... und binden ihn an unseren Port ...
UDPServerSocket.bind((localIP, localPort))

print("UDP server up and listening")

# ... und schon können wir auf Empfang gehen
# Die Funktion recvfrom() kehrt nur zurück, wenn eine Nachricht empfangen wurde
# oder ein Fehler aufgetreten ist ...
bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
# ... und liefert uns im Erfolgsfall ein Tupel mit diesem Inhalt:
message = bytesAddressPair[0]
address = bytesAddressPair[1]

# Die Nachricht und die Absender-IP schauen wir uns mal an...
print('Message from Client: ', message)
print('Client IP Address: ', address)

# ... und senden dann - freundlich wie wir sind - eine Antwort zurück
UDPServerSocket.sendto(bytesToSend, address)