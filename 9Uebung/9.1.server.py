import numpy as np
import socket
import matplotlib.pyplot as plt
import json

# Mit np.dtype definieren wir einen neuen NumPy-Datentyp ...
message_dtype = np.dtype([('id',    np.int32),
                         ('x', np.float32),
                         ('y', np.float32)])


# Wir nutzen das Loopback-Device zur rechnerinternen Kommunikation ...
localIP     = "127.0.0.1"
# ... und einen nicht belegten Port
localPort   = 1234
bufferSize = 1024
t_vec= []
sin_vec = []
class SinPlot():
  # Konstruktor
  def __init__(self):
    # Plot anlegen ...
    self.fig, self.ax = plt.subplots()
    
  # Aufruf für die erste Plot-Ausgabe
  def init_plot(self, t_vec, sin_vec):
    # Ersten Stützpunkt "normal" plotten ...
    self.sin_plot, = self.ax.plot(t_vec, sin_vec)

    # ... und das Diagramm groß genug machen für die weiteren Punkte
    self.ax.set_xlim(-1,2.0*np.pi)
    self.ax.set_ylim(-1.5,+1.5)
    
    
  # Aufruf für die weiteren Plot-Ausgaben
  def update_plot(self, t_vec, sin_vec):
    
    self.sin_plot.set_data(t_vec,sin_vec)
    plt.pause(0.00000001)




i = 0
UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
UDPServerSocket.bind((localIP, localPort))

print("UDP server up and listening")


while True:
    #bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
    #print(bytesAddressPair)
    #message_raw = bytesAddressPair[0]
    #print(len(message_raw))
    #messege = np.frombuffer(bytesAddressPair, dtype=message_dtype)
    
    bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
    message_medium_rare = json.loads(bytesAddressPair[0].decode('utf-8'))

    # Create a numpy array from the loaded JSON data with the correct dtype
    message_array = np.array([tuple(message_medium_rare[0])], dtype=message_dtype)

    # Access the first element of the numpy array
    message = message_array[0]
    
    if message["id"] == 1:
        my_plot = SinPlot()
        my_plot.init_plot(0,0)
        plt.pause(0.005)
        
    elif message["id"] == 2:
        t_vec.append(message["x"])
        sin_vec.append(message["y"])
        my_plot.update_plot(t_vec[:i], sin_vec[:i])
        i+=1
        
       
    else:
        print("Die Übertragung wurde beendet")
        break
        
        
    
    
    

