import socket
import numpy as np
import json
import time

# Mit np.dtype definieren wir einen neuen NumPy-Datentyp ...
message_dtype = np.dtype([('id',    np.int32),
                         ('x', np.float32),
                         ('y', np.float32)])



class Sinus():
  def __init__(self, A=1.0, omega=1.0):
    # Amplitude
    self.A = A
    # Kreisfrequenz
    self.omega = omega

  # "Setter"-Funktion für die Amplitude
  def set_A(self, A):
    self.A = A

  # "Setter"-Funktion für die Kreisfrequenz
  def set_omega(self, omega):
    self.omega = omega

  # Berechnung des Sinus
  def calc(self,t):
    return self.A * np.sin(self.omega*t)


def init(n_gesamt, t_gesamt, serverAddressPort):
    message1 = np.zeros(1, dtype=message_dtype)
    message1["id"] = 1
    message1["x"] = n_gesamt
    message1["y"] = t_gesamt
    bytesToSend = bytes(json.dumps(message1.tolist()), "UTF-8")
    #bytesToSend = bytes(str(message1), "UTF-8")
    UDPClientSocket.sendto(bytesToSend, serverAddressPort)
    
def Data():
    sinus = Sinus()
    omega = 0.0
    domega = 0.025
    A = 1.0
    dA = -0.001
    i=0
    sinus.set_A(A)
    sinus.set_omega(omega)
    message2 = np.zeros(1, dtype=message_dtype)
    message2["id"] = 2
    message2["x"] = t_vec[0]
    message2["y"] = sinus.calc(t_vec[0])
    bytesToSend = bytes(json.dumps(message2.tolist()), "UTF-8")
    #bytesToSend = bytes(str(message2), "UTF-8")
    UDPClientSocket.sendto(bytesToSend, serverAddressPort)
    
    
    for i in range(1, n_gesamt):
        t_vec[i] += t_vec[i-1]+dt
        omega += domega
        sinus.set_omega(omega)
        A += dA
        sinus.set_A(A)
        sin_vec[i] = sinus.calc(t_vec[i])
        message2["id"] = 2
        message2["x"] = t_vec[i]
        message2["y"] = sin_vec[i]
        print(t_vec[i])
        bytesToSend = bytes(json.dumps(message2.tolist()), "UTF-8")
        # bytesToSend = bytes(str(message2), "UTF-8")
        UDPClientSocket.sendto(bytesToSend, serverAddressPort)
        
        
def Terminate():
    message3 = np.zeros(1, dtype=message_dtype)
    message3["id"] = 3
    message3["x"] = 0
    message3["y"] = 0
    bytesToSend = bytes(json.dumps(message3.tolist()), "UTF-8")
    #bytesToSend = bytes(str(message3), "UTF-8")
    UDPClientSocket.sendto(bytesToSend, serverAddressPort)
    
if __name__ == "__main__":
    try:
        UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        print("UDP client up and sending")
        n_gesamt = 1000
        t_gesamt = 2.0 * np.pi
        dt = t_gesamt / n_gesamt
        # Vektor für Zeitwerte
        t_vec = np.zeros(n_gesamt)
        # Vektor für Ergebniswerte
        sin_vec = np.zeros_like(t_vec)
        serverAddressPort   = ("127.0.0.1", 1234)
        bufferSize  = 1024
        init(n_gesamt, t_gesamt, serverAddressPort)
        Data()
        Terminate()
    except Exception as inst:
                print("Exception aufgetreten", type(inst))    