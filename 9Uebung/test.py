import json

# Wir definieren ein Datenobjekt als Dictionary ...
input_data = {"Adresse": "Kleiststr.", "Hausnummer": 14}
print('Originale Daten:', input_data)

# ... und codieren diese mit dumps() in eine JSON-formatierte Byte-Folge um ...
json_string = bytes(json.dumps(input_data), 'UTF-8')
print('JSON codierte Daten:', json_string)

# .. die wir mit der 'loads()'-Methode wieder decodieren können
output_data = json.loads(json_string)
print('Decodierte JSON-Daten:', output_data)

# Wenn wir ein Datenobjekt im JSON-Format in eine Datei schreiben wollen,
# sieht das ganz ähnlich aus ...
with open('data.json', 'w', encoding='UTF-8') as f:
  json.dump(input_data, f, ensure_ascii=False, indent=4)

# ... und auch das Wieder-Einlesen aus der Datei ghet sehr leicht
with open('data.json') as f:
  loaded_data = json.load(f)

print('Aus der Datei geladener Datensatz:',  loaded_data)

#try:
#  f = open('nonsense.txt', 'r')
#  print('Datei erfolgreich geöffnet')
#
#except:
#    print('Exception')
  
  
try:
# Hier belegen wir 2 Variablen absichtlich 'falsch'
    a = 4
    b = 2
# Als gute Programmierer prüfen wir hier ab, ob die Voraussetzungen für die
# weiteren Berechnungen erfüllt sind.
    if a >= b:
        raise ValueError('a muss immer kleiner als b sein!')
# Hier ginge es weiter, wenn a < b wäre ...
except Exception as error:
    print('Exception:', error)